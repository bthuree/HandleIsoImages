The purpose of this small script is to make life easier if you are dealing with
a product that is delivered using 15 ISO images or so.

The first thing you need to do, is to create a release template, which contains the MD5SUM and ISO Name.

After that, you generate all the MD5SUM's as well as create the softlinks to the appropriate release directories.

The script will create a soft link from the iso image to the release directory, so
each release directory will only contain the images (soft link to iso directory) needed for this release.

./HandleIsoImages -m
Which will calculate MD5Sums on all new ISO images, as well as automatically create the soft links to the proper release directory.

./HandleIsoImages -f
To see which ISO images you still are missing (that is you need to download them).


bash-3.2# ./HandleIsoImages --help    
Usage: HandleIsoImages [options]
 
Options:
  -h, --help            show this help message and exit
  -e ENVFILE, --envfile=ENVFILE
                        Specify Environment File
  -m, --md5sums         Update md5sum database with new ISO images
  -c, --createlinks     Create release tree based on MFI files
  -f, --findmissing     Find Missing images based on MFI and md5sum database
  -r, --remove          Remove all images not in any MFI file
  -p, --print           List all releases
  -t TARGET, --target=TARGET
                        Together with remove, remove this release
  --force               Together with md5sums, re-calculate ALL images
                        md5sums. Together with remove, remove all surplus ISO
                        images. Together with remove & target, remove whole
                        release directory, not just the iso links.
                        
bash-3.2# ./HandleIsoImages -r   
Following ISO images is not referenced by any release directory, and hence can be removed.
If you re-run this command with the --force option, they will be removed, and the md5 database updated.
['slask', '19089-CXP9027383_Ux_H2.iso', 'OAM_Services_for_X86_media_for_16.0.8.iso', 'Consolidated_Solaris_U10_X86_for_HP_Hardware_16A.iso']