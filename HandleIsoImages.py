#!/usr/bin/python

# Script to manage ISO Images

from optparse import OptionParser 
import sys
import os.path
import subprocess
import hashlib
import logging
import ConfigParser
import shutil

MAIN_DIR="/JUMP/releases/"
MAIN_DIR="/JUMP/scripts/slask/"
TMPFILE = "/tmp/_tmp_file.%d" % os.getpid()
MISSINGIMAGES="MissingImages.txt"
ENVFILE=os.path.basename(__file__) +".cfg"
TRACE_LEVEL=logging.INFO

# Logger Levels
# CRITICAL, ERROR, WARNING, INFO, DEBUG, NOTSET
logging.basicConfig(level=TRACE_LEVEL)
logger = logging.getLogger(__name__)

def DefineLibraryPaths (MainDir):
	global ISO_DIR
	global DB_DIR
	global DB
	ISO_DIR=MAIN_DIR + "iso" + "/"
	DB_DIR=MAIN_DIR + "db" + "/"
	DB=DB_DIR + "md5sums.db"


def SetupCmdParser():
	logger.debug ("In SetupCmdParser")
	parser = OptionParser()			
	parser.add_option("-e", "--envfile",
					  action="store", type="string", dest="envfile", 
					  help="Specify Environment File")
	parser.add_option("-m", "--md5sums",
					  action="store_true", dest="md5sums", default=False,
					  help="Update md5sum database with new ISO images")
	parser.add_option("-c", "--createlinks",
					  action="store_true", dest="createlinks", default=False,
					  help="Create release tree based on MFI files")
	parser.add_option("-f", "--findmissing",
					  action="store_true", dest="missing", default=False,
					  help="Find Missing images based on MFI and md5sum database")
	parser.add_option("-r", "--remove",
					  action="store_true", dest="remove", default=False,
					  help="Remove all images not in any MFI file")
	parser.add_option("-p", "--print",
					  action="store_true", dest="printall", default=False,
					  help="List all releases")
	parser.add_option("-t", "--target",
					  action="store", type="string", dest="target",
					  help="Together with remove, remove this release")
	parser.add_option("", "--force",
					  action="store_true", dest="force", default=False,
					  help="Together with md5sums, re-calculate ALL images md5sums. "
					  "Together with remove, remove all surplus ISO images. "
					  "Together with remove & target, remove whole release directory, not just the iso links. ")
	return parser

def ValidateInputCmds (options, args):
	global ENVFILE
	logger.debug ("In ValidateInputCmds")
	if options.md5sums + options.createlinks + options.missing + options.remove + options.printall != 1:
		parser.error ("Only one of md5sums, createlinks, findmissing, remove, print")
	if options.force and (options.md5sums + options.remove != 1):
		parser.error ("Force only allowed together with md5sums, or remove")
#	Check environment file exists
	logger.debug ("ENVFILE 1 %s", ENVFILE)
	logger.debug ("Will set envfile variable %s", options.envfile)
	if (options.envfile ):
		logger.debug ("Ok, Environment file has been specified ")
		if (not os.path.isfile (options.envfile)):
			parser.error ("Environment file do not exists : %s" % options.envfile)
		else:
			logger.debug ("Ok, and Environment file exists...")
			ENVFILE=options.envfile
	logger.debug ("ENVFILE 2 %s", ENVFILE)



def PrintInputOptions(options):
	global MAIN_DIR
	logger.debug ("Input options %s", options)
	logger.debug ("ENVFILE  %s", ENVFILE)
	logger.debug ("Main Dir	%s", MAIN_DIR)
	logger.debug ("ISO Dir	%s", ISO_DIR)
	logger.debug ("DB_DIR	%s", DB_DIR)
	logger.debug ("DB	%s", DB)

def ReadEnvironmentFile(EnvFile):
	global MAIN_DIR
	global TRACE_LEVEL
	logger.debug ("ENVFILE 3 %s", ENVFILE)
	if (os.path.isfile (EnvFile)):
		logger.debug ("Reading data from Environment File = %s " % EnvFile)
		config = ConfigParser.ConfigParser()
		config.read(ENVFILE)
		MAIN_DIR=config.get('Paths', 'MAIN_DIR')
		if not (MAIN_DIR.endswith("/")):
			MAIN_DIR=MAIN_DIR+"/"
		TRACE_LEVEL=config.get('Trace', 'TRACE_LEVEL')
		logger.debug ("MAIN_DIR = %s " % MAIN_DIR)
		logger.debug ("TRACE_LEVEL = %s " % TRACE_LEVEL)
		if   (TRACE_LEVEL=='CRITICAL'):
			logger.setLevel (logging.CRITICAL)
		elif (TRACE_LEVEL=='ERROR'):
			logger.setLevel (logging.ERROR)
		elif (TRACE_LEVEL=='WARNING'):
			logger.setLevel (logging.WARNING)
		elif (TRACE_LEVEL=='INFO'):
			logger.setLevel (logging.INFO)
		elif (TRACE_LEVEL=='DEBUG'):
			logger.setLevel (logging.DEBUG)
		else:
			logger.setLevel (logging.NOTSET)
	else:
		logger.debug ("Could not find Environment File = %s " % EnvFile)
	DefineLibraryPaths(MAIN_DIR)
	
def VerifyTargetFile():
	if (options.target):
		logger.debug ("Ok, Target file has been specified ")
		if (not os.path.isfile (DB_DIR + options.target + ".MFI")):
			parser.error ("Target file do not exists : %s" % options.target)
		else:
			logger.debug ("Ok, and Target file exists...")

def ListAllReleaseMFI():
	logger.debug ("In ListAllReleaseMFI")
	MFI_Dir=os.listdir(DB_DIR)
	MFI_FILES=[i for i in MFI_Dir if i.endswith(".MFI")];
	MFI_FILES.sort()
	print "Currently supporting following releases"
	for filename in MFI_FILES:
		print "	Release %s " % filename[:-len(".MFI")]

def ShowMissingImages():
	logger.debug ("In ShowMissingImages")
	MFI_Dir=os.listdir(DB_DIR)
	MFI_FILES=[i for i in MFI_Dir if i.endswith(".MFI")];
	MFI_FILES.sort()
	for filename in MFI_FILES:
		writeMFIheader = True
		FULL_MFI_NAME="%s/%s" % (DB_DIR, filename)
		with open(FULL_MFI_NAME, 'r') as ifile:
			for line in ifile:
				cols=line.split()
				MD5SUM=cols[0]
				result = subprocess.call (["grep", "-i", MD5SUM, DB], stdout = open ( TMPFILE, 'w') )
				if (result == 1):
					if (writeMFIheader):
						print ""
						print "MFI File : %s " % FULL_MFI_NAME
						writeMFIheader = False
					print line.strip()

def RemoveFile(FileToRemove):
	logger.debug ("In RemoveFile")
	if os.path.isfile(FileToRemove):
		os.remove(FileToRemove)
		
def CreateAllMd5Sums():
	logger.debug ("In CreateAllMd5Sums")
	RemoveFile (DB)
	ISO_DIR_CONTENT=os.listdir(ISO_DIR)
	#ISO_FILES=[i for i in ISO_DIR_CONTENT if i.endswith(".iso")];
	ISO_FILES=[i for i in ISO_DIR_CONTENT ];
	print "Will re-generate md5sums for %s iso images" % len(ISO_FILES)
	for isoname in ISO_FILES:
		result = subprocess.call(["digest",  "-a", "md5", ISO_DIR + isoname ], stdout = open( TMPFILE, 'w') )
		if (result == 0):
			f = open(TMPFILE, 'r')
			line = f.readline()
			f.close()
			md5sum = line.strip()
			print "Added md5sum %s for file %s " % (md5sum, isoname)
			f = open(DB, 'a')
			f.write("%s	%s \n" % (md5sum, isoname))
			f.close()
		RemoveFile (TMPFILE)

def CreateMissingMd5Sums():
	logger.debug ("In CreateMissingMd5Sums")
	devnull = open('/dev/null', 'w')	
	ISO_DIR_CONTENT=os.listdir(ISO_DIR)
	#ISO_FILES=[i for i in ISO_DIR_CONTENT if i.endswith(".iso")];
	ISO_FILES=[i for i in ISO_DIR_CONTENT ];
	
	Missing_Names=[]
	for isoname in ISO_FILES:
		result = subprocess.call (["grep",  isoname, DB], stdout=devnull)
		if (result == 1):
			Missing_Names+=[isoname]
		if (result == 2):
				print "grep result = %i " % result
	print "Will generate md5sums for %s iso images" % len(Missing_Names)
	for isoname in Missing_Names:
		result = subprocess.call(["digest",  "-a", "md5", ISO_DIR + isoname ], stdout = open( TMPFILE, 'w') )
		if (result == 0):
			f = open(TMPFILE, 'r')
			line = f.readline()
			f.close()
			md5sum = line.strip()
			print "Added md5sum %s for file %s " % (md5sum, isoname)
			f = open(DB, 'a')
			f.write("%s	%s \n" % (md5sum, isoname))
			f.close()
		else:
			print "Digest returned %i with %s " % (result, isoname)
	RemoveFile (TMPFILE)

def RemoveFileFromMD5DB (isoname):
	logger.debug ("In RemoveFileFromMD5DB - Will remove %s" % isoname)
	result = subprocess.call (["grep",  "-v", isoname, DB], stdout = open( TMPFILE, 'w'))
	shutil.copy (TMPFILE, DB)

def ensure_dir(DIR):
	logger.debug ("In ensure_dir")
	if not os.path.exists(DIR):
		os.makedirs(DIR)

def CreateReleaseTree():
	logger.debug ("In CreateReleaseTree")

def PopulateReleaseDir (MFI_FileName, ReleaseName):
	logger.debug ("In PopulateReleaseDir")
	with open(MFI_FileName, 'r') as ifile:
		MissingImages=False
		RemoveFile (MAIN_DIR+ReleaseName+"/"+MISSINGIMAGES)
		for line in ifile:
			cols=line.split()
			MD5SUM=cols[0]
			TYPE=cols[2]
			NBR=cols[3]
			NAME=cols[4]
			result = subprocess.call (["grep", "-i", MD5SUM, DB], stdout = open ( TMPFILE, 'w') )
			if (result == 0):
				f = open(TMPFILE, 'r')
				line = f.readline()
				f.close()
				ISO_MD5SUM,ISO_FILENAME = line.split()
				NEWNAME=TYPE+"_"+NBR+"_"+NAME+"_"+ISO_FILENAME
				RELPATH=os.path.relpath (ISO_DIR, MAIN_DIR+ReleaseName)
				if not os.path.exists(MAIN_DIR+ReleaseName+"/"+NEWNAME):
					os.symlink (RELPATH+"/"+ISO_FILENAME, MAIN_DIR+ReleaseName+"/"+NEWNAME)
			if (result != 0):
				f = open(MAIN_DIR+ReleaseName+"/"+MISSINGIMAGES, 'a')
				f.write("%s\n" % line.strip())
				f.close()
				MissingImages=True
		if (MissingImages):
			print "	Missing images in release %s" % ReleaseName

def CreateRelease():
	logger.debug ("In CreateRelease")
	MFI_Dir=os.listdir(DB_DIR)
	MFI_FILES=[i for i in MFI_Dir if i.endswith(".MFI")];
	MFI_FILES.sort()
	print ""
	for filename in MFI_FILES:
		releasename = filename[:-len(".MFI")]
		print "Populating %s " % releasename
		ensure_dir (MAIN_DIR + releasename)
		PopulateReleaseDir (DB_DIR + filename, releasename)
	
def RemoveNotUsedImages():
	logger.debug ("In RemoveNotUsedImages")
	ISO_DIR_CONTENT=os.listdir(ISO_DIR)
	#ISO_FILES=[i for i in ISO_DIR_CONTENT if i.endswith(".iso")];
	ISO_FILES=[i for i in ISO_DIR_CONTENT ];

	delete_iso=[]
	for isoname in ISO_FILES:
		ISO_is_used=False
		RELEASE_DIRS=os.listdir(MAIN_DIR)
		# Remove certain directories from the list. If they do not exist, ValueError will be raised. Ignore this one.
		try: 
			RELEASE_DIRS.remove ('iso')
		except ValueError:
			pass
		try:
			RELEASE_DIRS.remove ('db')
		except ValueError:
			pass
		try:
			RELEASE_DIRS.remove ('script')
		except ValueError:
			pass
		for release in RELEASE_DIRS:
			release_content=os.listdir(MAIN_DIR + release)
			for name in release_content:
				if name.endswith(isoname):
					ISO_is_used=True
		if not ISO_is_used:
			delete_iso += [isoname]
	if (delete_iso):
		if (options.force):
			for filename in delete_iso:
				print "Removing iso image %s%s " % (ISO_DIR,filename)
				RemoveFile (ISO_DIR + filename)
				RemoveFileFromMD5DB (filename)
		else:
			print "Following ISO images is not referenced by any release directory, and hence can be removed."
			print "If you re-run this command with the --force option, they will be removed, and the md5 database updated."
			print delete_iso
	else:
		print "Nothing to delete, all iso images are refered to"

def RemoveIsoLinks (target_dir):
	logger.debug ("In RemoveIsoLinks, will remove ISO links in %s " % target_dir)
	TARGET_CONTENT=os.listdir(target_dir)
	for TARGET_FILE in TARGET_CONTENT:
		FileToRemove = target_dir + "/" + TARGET_FILE
#		if TARGET_FILE.endswith(".iso") and os.path.islink (FileToRemove):
		if os.path.islink (FileToRemove):
			logger.debug ("Remove link %s " % FileToRemove)
			if os.path.isfile(FileToRemove):
				os.remove(FileToRemove)
		else:
			logger.debug ("File %s is not a link" % FileToRemove)

def RemoveDirectoryTree (target_dir):
	logger.debug ("In RemoveDirectoryTree, will remove directory tree %s " % target_dir)
	shutil.rmtree (target_dir, 1)

def RemoveRelease(release_target):
	logger.debug ("In RemoveRelease, will remove %s " % release_target)
	# First remove the MFI file
	RemoveFile (DB_DIR + release_target + ".MFI")
	# If force, then remove the whole target directory
	if (options.force):
		RemoveDirectoryTree (MAIN_DIR + release_target)
	else:
		RemoveIsoLinks (MAIN_DIR + release_target)
	RemoveNotUsedImages

def ExcecuteCmds (options):
	logger.debug ("In ExcecuteCmds")
	#if (options.enable):
	#	   _ChangeManagedTo = "1"
	#if (options.disable):
	#	   _ChangeManagedTo = "0"

	if (options.printall):
		ListAllReleaseMFI ()
	elif (options.md5sums):
		if (options.force):
			CreateAllMd5Sums ()
		else:
			CreateMissingMd5Sums ()
		CreateRelease()
	elif (options.createlinks):
		CreateRelease()
	elif (options.missing):
		ShowMissingImages()
	elif (options.remove):
		if (options.target):
			RemoveRelease (options.target)
		else:
			RemoveNotUsedImages()
			
parser=SetupCmdParser()
(options, args) = parser.parse_args()
ValidateInputCmds(options, args) 
logger.debug ("ENVFILE 4 %s", ENVFILE)
ReadEnvironmentFile(ENVFILE)
VerifyTargetFile()	## Need to do it after Environment has been finalized
PrintInputOptions(options)

ExcecuteCmds(options)	   
